package nl.mondriaan.ict.calculator.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import nl.mondriaan.ict.calculator.R;
import nl.mondriaan.ict.calculator.model.Calculator;

public class CalculatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        Calculator calculatorModel = new Calculator();
        calculatorModel.addNumber("2");
        calculatorModel.addOperator("+");
        calculatorModel.addNumber("2");
        calculatorModel.addOperator("-");
        calculatorModel.addNumber("3");
        calculatorModel.addOperator("*");
        calculatorModel.addNumber("10");
        calculatorModel.addOperator("/");
        calculatorModel.addNumber("2");


        TextView resultTextView = (TextView)findViewById(R.id.textCurrentCalcView);
        resultTextView.setText("" + calculatorModel.getResult());

        TextView formulaTextView = (TextView)findViewById(R.id.textFormulaView);
        formulaTextView.setText("" + calculatorModel.getFormula());
        Log.d("TEST", calculatorModel.getResult() + "");
    }
}
