package nl.mondriaan.ict.calculator.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rcspringer on 20-4-17.
 */

public class Calculator {

    private List<Double> numbers;
    private List<String> operators;

    private double resultNumber = 0;

    public Calculator() {
        numbers = new ArrayList<Double>();
        operators = new ArrayList<String>();
    }

    public void addNumber(String number) {
        try {
            double calculatorNumber = Double.parseDouble(number);
            // Alleen als het de eerste nummbr moeten we
            if(numbers.size() == 0) {
                resultNumber = calculatorNumber;
            }
            numbers.add(calculatorNumber);
        } catch (Exception e) {
            // Getal is geen number
        }
    }

    public void addOperator(String operator) {
        if(operator.equals("+") || operator.equals("-") || operator.equals("/") || operator.equals("*")) {
            operators.add(operator);

        } else {
            // Geen geldige operator
        }
    }

    public double getResult() {
        int numberIndex = 0;
        double resultNumber = numbers.get(numberIndex);
        numberIndex++;
        for (int i = 0; i < operators.size(); i++) {
            if(numberIndex >= numbers.size())  {
                break;
            }
            String currentOperator = operators.get(i);

            if (currentOperator.equals("+")) {
                resultNumber = sum(resultNumber, numbers.get(numberIndex));
            }
            else if(currentOperator.equals("-")) {
                resultNumber = subtract(resultNumber, numbers.get(numberIndex));
            }
            else if(currentOperator.equals("/")) {
                resultNumber = divide(resultNumber, numbers.get(numberIndex));
            }
            else if(currentOperator.equals("*")) {
                resultNumber = multiply(resultNumber, numbers.get(numberIndex));
            }
            numberIndex++;
        }

        return resultNumber;
    }

    public String getFormula() {
        int length = Math.max(numbers.size(), operators.size());
        String formula = "";
        Log.d("TEST", "" +length);
        for(int i = 0; i < length; i++) {
            if(i < numbers.size() - 1) {
                formula += " ";
                formula += numbers.get(i);
            }
            if(i < operators.size() - 1) {
                formula += " " + operators.get(i);
            }
        }
        return formula;
    }

    public double sum(double value1, double value2) {
        return value1 + value2;
    }

    public double subtract(double value1, double value2) {
        return value1 - value2;
    }

    public double multiply(double value1, double value2) {
        return value1 * value2;
    }

    public double divide(double value1, double value2) {
        return value1 / value2;
    }
}
